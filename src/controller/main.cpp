#include <ros/ros.h>
#include <string>

#include <assembly_control_ros/robot_input.h>
#include <assembly_control_ros/robot_output.h>
#include <assembly_control_ros/evacuation_conveyor_input.h>
#include <assembly_control_ros/evacuation_conveyor_output.h>
#include <assembly_control_ros/supply_conveyor_input.h>
#include <assembly_control_ros/supply_conveyor_output.h>
#include <assembly_control_ros/assembly_station_input.h>
#include <assembly_control_ros/assembly_station_output.h>
#include <assembly_control_ros/camera_input.h>
#include <assembly_control_ros/camera_output.h>


class Controller_class {
    public : 
        Controller_class(ros::NodeHandle node) : node_(node) {
            // Topic names
            const std::string& robot_topic               = "robot";
            const std::string& evacuation_conveyor_topic = "evacuation_conveyor";
            const std::string& supply_conveyor_topic     = "supply_conveyor";
            const std::string& assembly_station_topic    = "assembly_station";
            const std::string& camera_topic              = "camera";
            // Publish into machine input topic
            robot_input_publisher_ = node.advertise<assembly_control_ros::robot_input>(
                robot_topic + "/input", 10);
            evacuation_conveyor_input_publisher_ = node.advertise<assembly_control_ros::evacuation_conveyor_input>(
                evacuation_conveyor_topic + "/input", 10);
            supply_conveyor_input_publisher_ = node.advertise<assembly_control_ros::supply_conveyor_input>(
                supply_conveyor_topic + "/input", 10);
            assembly_station_input_publisher_ = node.advertise<assembly_control_ros::assembly_station_input>(
                assembly_station_topic + "/input", 10);
            camera_input_publisher_ = node.advertise<assembly_control_ros::camera_input>(
                camera_topic + "/input", 10);
            // Subscribe to machine output topic
            robot_output_subscriber_ = node.subscribe<assembly_control_ros::robot_output>(
                robot_topic + "/output", 10, &Controller_class::outputRobotCallback, this);
            evacuation_conveyor_output_subscriber_ = node.subscribe(
                evacuation_conveyor_topic + "/output", 10, &Controller_class::outputEvacuationCallback, this);
            supply_conveyor_output_subscriber_ = node.subscribe(
                supply_conveyor_topic + "/output", 10, &Controller_class::outputSupplyCallback, this);
            assembly_station_output_subscriber_ = node.subscribe(
                assembly_station_topic + "/output", 10, &Controller_class::outputAssemblyCallback, this);
            camera_output_subscriber_ = node.subscribe(
                camera_topic + "/output", 10, &Controller_class::outputCameraCallback, this);
        }
    protected : 
        // Send input to machine
        void sendRobotInputs(assembly_control_ros::robot_input input_message) {
            robot_input_publisher_.publish(input_message); }
        void sendSupplyInputs(assembly_control_ros::supply_conveyor_input input_message) {
            supply_conveyor_input_publisher_.publish(input_message); }
        void sendEvacuationInputs(assembly_control_ros::evacuation_conveyor_input input_message) {
            evacuation_conveyor_input_publisher_.publish(input_message); }
        void sendAssemblyInputs(assembly_control_ros::assembly_station_input input_message) {
            assembly_station_input_publisher_.publish(input_message); }
        void sendCameraInputs(assembly_control_ros::camera_input input_message) {
            camera_input_publisher_.publish(input_message); }
        // Get output from machine
        assembly_control_ros::robot_output& getRobotOutputs() {
            return robot_output_message_; }
        assembly_control_ros::evacuation_conveyor_output& getEvacuationOutputs() {
            return evacuation_conveyor_output_message_; }
        assembly_control_ros::supply_conveyor_output& getSupplyOutputs() {
            return supply_conveyor_output_message_; }
        assembly_control_ros::assembly_station_output& getAssemblyOutputs() {
            return assembly_station_output_message_; }
        assembly_control_ros::camera_output& getCameraOutputs() {
            return camera_output_message_; }
    private :
        // Machines outputs callback
        void outputRobotCallback(const assembly_control_ros::robot_output::ConstPtr& message) {
            robot_output_message_ = *message; }
        void outputEvacuationCallback(const assembly_control_ros::evacuation_conveyor_output::ConstPtr& message) {
            evacuation_conveyor_output_message_ = *message; }
        void outputSupplyCallback(const assembly_control_ros::supply_conveyor_output::ConstPtr& message) {
            supply_conveyor_output_message_ = *message; }
        void outputAssemblyCallback(const assembly_control_ros::assembly_station_output::ConstPtr& message) {
            assembly_station_output_message_ = *message; }
        void outputCameraCallback(const assembly_control_ros::camera_output::ConstPtr& message) {
            camera_output_message_ = *message; }
        // Init node, publishers, subscriber
        ros::NodeHandle node_;
        // Init publichers
        ros::Publisher  robot_input_publisher_;
        ros::Publisher  evacuation_conveyor_input_publisher_;
        ros::Publisher  supply_conveyor_input_publisher_;
        ros::Publisher  assembly_station_input_publisher_;
        ros::Publisher  camera_input_publisher_;
        // Init subscribers
        ros::Subscriber robot_output_subscriber_;
        ros::Subscriber evacuation_conveyor_output_subscriber_;
        ros::Subscriber supply_conveyor_output_subscriber_;
        ros::Subscriber assembly_station_output_subscriber_;
        ros::Subscriber camera_output_subscriber_;
        // Init output messages
        assembly_control_ros::robot_output                  robot_output_message_;
        assembly_control_ros::evacuation_conveyor_output    evacuation_conveyor_output_message_;
        assembly_control_ros::supply_conveyor_output        supply_conveyor_output_message_;
        assembly_control_ros::assembly_station_output       assembly_station_output_message_;
        assembly_control_ros::camera_output                 camera_output_message_;
};

class Controller 
    : public Controller_class {

    public : 
        Controller(ros::NodeHandle node)
            : Controller_class(node), state_supply_(StateSupply::Start) {
        }

        void process() {
            assembly_control_ros::robot_input                   robot_input;
            assembly_control_ros::evacuation_conveyor_input     evacuation_input;
            assembly_control_ros::supply_conveyor_input         supply_input;
            assembly_control_ros::assembly_station_input        assembly_input;
            assembly_control_ros::camera_input                  camera_input;

            auto& robot_outputs      = getRobotOutputs();
            auto& evacuation_outputs = getEvacuationOutputs();
            auto& supply_outputs     = getSupplyOutputs();
            auto& assembly_outputs   = getAssemblyOutputs();
            auto& camera_outputs     = getCameraOutputs();

            switch (state_supply_) {
                case StateSupply::Start:
                    if (supply_outputs.part_available) {
                        ROS_INFO("[Controller] Part Available");
                        state_supply_ = StateSupply::Recognition;
                    }
                    break;
                case StateSupply::Recognition:
                    ROS_INFO("[Controller] Cam recognition");
                    camera_input.start_recognition = true;
                    sendCameraInputs(camera_input);
                    state_supply_ = StateSupply::Right;
                    break;
                case StateSupply::Right:
                    ROS_INFO("[Controller] Robot right");
                    robot_input.go_right = true;
                    state_supply_ = StateSupply::Grasp; 
                    sendRobotInputs(robot_input);
                    break;
                case StateSupply::Grasp:
                    if (robot_outputs.at_supply_conveyor) {
                        ROS_INFO("[Controller] Robot grasp");
                        robot_input.grasp = true;
                        sendRobotInputs(robot_input);
                        state_supply_ = StateSupply::Decision; 
                    }
                    break;
                case StateSupply::Decision:
                    // Process memories and make decision
                    if (camera_outputs.part_analyzed && robot_outputs.grasped && state_assembling_ != StateAssembling::Evacuation) {
                        ROS_INFO("[Controller] Decision");
                        camera_outputs.part_analyzed = false;
                        robot_outputs.grasped = false;
                        // Decision either evacuation or assembling
                        if (!camera_outputs.part1 && !camera_outputs.part2 && !camera_outputs.part3) {
                            ROS_INFO("[Controller] Go evacuation -> Unknown part");
                            state_evacuation_ = StateEvacuation::StopEC;
                        }
                        if (camera_outputs.part1) {
                            camera_outputs.part1 = false;
                            if (cell1.getCell()) {
                                ROS_INFO("[Controller] Go evacuation");
                                state_evacuation_ = StateEvacuation::StopEC;
                            }
                            else {
                                cell1.switchToDo();
                                cell1.switchCell();
                                ROS_INFO("[Controller] Go assembling");
                                state_assembling_ = StateAssembling::Left;
                            }
                        }
                        if (camera_outputs.part2) {
                            camera_outputs.part2 = false;
                            if (cell2.getCell()) {
                                ROS_INFO("[Controller] Go evacuation");
                                state_evacuation_ = StateEvacuation::StopEC;
                            }
                            else {
                                cell2.switchToDo();
                                cell2.switchCell();
                                ROS_INFO("[Controller] Go assembling");
                                state_assembling_ = StateAssembling::Left;
                            }
                        }
                        if (camera_outputs.part3) {
                            camera_outputs.part3 = false;
                            if (cell3.getCell()) {
                                ROS_INFO("[Controller] Go evacuation");
                                state_evacuation_ = StateEvacuation::StopEC;
                            }
                            else {
                                cell3.switchToDo();
                                cell3.switchCell();
                                ROS_INFO("[Controller] Go assembling");
                                state_assembling_ = StateAssembling::Left;
                            }
                        }
                        ROS_INFO("State supply : Default");
                        state_supply_ = StateSupply::Default;
                    }
                    break;
                case StateSupply::Default:
                    break;
            }

            // Go Assembling
            switch (state_assembling_) {
                case StateAssembling::Default:
                    break;
                case StateAssembling::Left:
                    robot_input.go_left = true;
                    state_assembling_ = StateAssembling::StartSC; 
                    sendRobotInputs(robot_input);
                    break;
                case StateAssembling::StartSC:
                    if (robot_outputs.at_assembly_station) {
                        ROS_INFO("[Controller] Robot at assembly station");
                        ROS_INFO("[Controller] Start supply conveyor");
                        supply_input.restart = true;
                        sendSupplyInputs(supply_input);
                        state_assembling_ = StateAssembling::Assembling;
                    }
                    break;
                case StateAssembling::Assembling:
                        robot_outputs.at_assembly_station = false;
                        if (cell1.getToDo()) {
                            robot_input.assemble_p1 = true;
                            sendRobotInputs(robot_input);
                            cell1.switchToDo();
                            ROS_INFO("[Controller] Part 1 assembling");
                        }
                        if (cell2.getToDo()) {
                            robot_input.assemble_p2 = true;
                            sendRobotInputs(robot_input);
                            cell2.switchToDo();
                            ROS_INFO("[Controller] Part 2 assembling");
                        }
                        if (cell3.getToDo()) {
                            robot_input.assemble_p3 = true;
                            sendRobotInputs(robot_input);
                            cell3.switchToDo();
                            ROS_INFO("[Controller] Part 3 assembling");
                        }
                        if (robot_outputs.done_p1 || robot_outputs.done_p2 || robot_outputs.done_p3) {
                            robot_outputs.done_p1 = false;
                            robot_outputs.done_p2 = false;
                            robot_outputs.done_p3 = false;
                            ROS_INFO("[Controller] Part assembled");
                            state_assembling_ = StateAssembling::Checking;
                        }
                    break;
                case StateAssembling::Checking:
                    // All parts done
                    if (cell1.getCell() && cell2.getCell() && cell3.getCell()) {
                        assembly_input.all_parts_available = true;
                        sendAssemblyInputs(assembly_input);
                        ROS_INFO("[Controller] Assembling evacuation");
                        state_assembling_ = StateAssembling::Evacuation;
                    }
                    else {
                        ROS_INFO("[Controller] Restart - Slot still available");
                        state_assembling_ = StateAssembling::Default;
                    }
                    state_supply_ = StateSupply::Start;
                    break;
                case StateAssembling::Evacuation:
                    if (assembly_outputs.ready) {
                        assembly_outputs.ready = false;
                        // Reset all memories
                        cell1.resetCell();
                        cell2.resetCell();
                        cell3.resetCell();
                        ROS_INFO("[Controller] Station ready - memory reset");
                        ROS_INFO("State assembling :  Default");
                        state_assembling_ = StateAssembling::Default;
                    }
                    break;
            }

            // Go Evacuation
            switch (state_evacuation_) {
                case StateEvacuation::Default:
                    break;
                case StateEvacuation::StopEC:
                    ROS_INFO("[Controller] Stop evacuation conveyor");
                    evacuation_input.stop = true;
                    sendEvacuationInputs(evacuation_input);
                    state_evacuation_ = StateEvacuation::Right;
                    break;
                case StateEvacuation::Right:
                    ROS_INFO("[Controller] Robot right");
                    robot_input.go_right = true;
                    sendRobotInputs(robot_input);
                    state_evacuation_ = StateEvacuation::StartSC;
                    break;
                case StateEvacuation::StartSC:
                    if (robot_outputs.at_evacuation_conveyor && evacuation_outputs.stopped) {
                        ROS_INFO("[Controller] Start supply conveyor");
                        robot_outputs.at_evacuation_conveyor = false;
                        evacuation_outputs.stopped = false;
                        supply_input.restart = true;
                        sendSupplyInputs(supply_input);
                        state_evacuation_ = StateEvacuation::Release;
                    }
                    break;
                case StateEvacuation::Release:
                    ROS_INFO("[Controller] Robot release");
                    robot_input.release = true;
                    sendRobotInputs(robot_input);
                    state_evacuation_ = StateEvacuation::StartEC;
                    break;
                case StateEvacuation::StartEC:
                    if (robot_outputs.released) {
                        ROS_INFO("[Controller] Go assembly");
                        robot_outputs.released = false;
                        evacuation_input.restart = true;
                        sendEvacuationInputs(evacuation_input);
                        state_evacuation_ = StateEvacuation::Left;
                    }
                    break;
                case StateEvacuation::Left:
                    robot_input.go_left = true;
                    if (robot_outputs.at_assembly_station) {
                        ROS_INFO("[Controller] Restart");
                        robot_input.go_left = false;
                        // Restart Supply sequence
                        state_supply_ = StateSupply::Start;
                        ROS_INFO("[Controller] supply_state::Start");
                        state_evacuation_ = StateEvacuation::Default;
                        ROS_INFO("State evacuation : Default");
                    }
                    sendRobotInputs(robot_input);
                    break;
            }
        }

    private :
        enum class StateSupply     { Start, Recognition, Right, Grasp, Decision, Default };
        enum class StateAssembling { Default, Left, StartSC, Assembling, Checking, Evacuation };
        enum class StateEvacuation { Default, StopEC, Right, StartSC, Release, StartEC, Left };
        StateSupply state_supply_;
        StateAssembling state_assembling_;
        StateEvacuation state_evacuation_;
        class Memory {
            public :
                void switchCell() { cell = !cell; }
                bool getCell()    { return cell; }
                void resetCell()  { cell = false; }

                void switchToDo() { to_do = ! to_do; }
                bool getToDo()    { return to_do; }
            private :
                bool cell = false;
                bool to_do = false;
        };
        Memory cell1, cell2, cell3;
};


int main(int argc, char* argv[]) {
    ros::init(argc, argv, "controller");

    ros::NodeHandle node;

    ros::Rate loop_rate(50);

    Controller controller(node);

    while (ros::ok()) {
        controller.process();

        ros::spinOnce();

        loop_rate.sleep();
    }

}
