#include <ros/ros.h>

#include <assembly_control_ros/robot_state.h>
#include <assembly_control_ros/robot_command.h>
#include <assembly_control_ros/robot_input.h>
#include <assembly_control_ros/robot_output.h>

#include <common/machine_controller.hpp>



class Robot
    : public MachineController<assembly_control_ros::robot_state,
                               assembly_control_ros::robot_input,
                               assembly_control_ros::robot_command,
                               assembly_control_ros::robot_output> {
public:
    Robot(ros::NodeHandle node)
        : MachineController(node, "robot"), state_(State::Idle) {
    }

    void location_update(bool * location, bool show_info = false) {
        if (getState().at_assembly_station) {
            if (show_info) 
                ROS_INFO(" [Robot] At assembly");
            location[0] = 1;
            location[1] = 0;
            location[2] = 0;
        }
        if (getState().at_supply_conveyor) {
            if (show_info) 
                ROS_INFO(" [Robot] At supply");
            location[0] = 0;
            location[1] = 1;
            location[2] = 0;
        }
        if (getState().at_evacuation_conveyor) {
            if (show_info) 
                ROS_INFO(" [Robot] At evacuation");
            location[0] = 0;
            location[1] = 0;
            location[2] = 1;
        }
        if (!(getState().at_assembly_station || getState().at_supply_conveyor || getState().at_evacuation_conveyor)) {
            if (show_info) 
                ROS_INFO(" [Robot] In movement");
            location[0] = 0;
            location[1] = 0;
            location[2] = 0;
        }
    }

    virtual void process() override {
        assembly_control_ros::robot_command commands;
        assembly_control_ros::robot_output outputs;

        auto& inputs = getInputs();

        switch (state_) {

            case State::Idle:
                // Send location outputs
                if (getState().at_assembly_station) 
                    outputs.at_assembly_station = true;
                if (getState().at_supply_conveyor) 
                    outputs.at_supply_conveyor = true;
                if (getState().at_evacuation_conveyor) 
                    outputs.at_evacuation_conveyor = true;
                sendOuputs(outputs);

                // Update previous location
                location_update(previous_location);

                // Go right
                if (inputs.go_right) {
                    inputs.go_right = false;
                    if (getState().at_evacuation_conveyor)
                        ROS_INFO(" [Robot] Error : Already at the evacuation");
                    else {
                        ROS_INFO(" [Robot] Going right");
                        ROS_INFO(" [Robot] In movement");
                        state_ = State::Right;
                    }
                }

                // Go left
                if (inputs.go_left) {
                    inputs.go_left = false;

                    if (getState().at_assembly_station)
                        ROS_INFO(" [Robot] Error : Already at the assembly");
                    else {
                        ROS_INFO(" [Robot] Going left");
                        ROS_INFO(" [Robot] In movement");
                        state_ = State::Left;
                    }
                }

                // Grasp
                if (inputs.grasp) {
                    inputs.grasp = false;
                    ROS_INFO(" [Robot] Grasp in progress");
                    state_ = State::Grasp;
                }

                // Release
                if (inputs.release) {
                    inputs.release = false;
                    ROS_INFO(" [Robot] Release in progress");
                    state_ = State::Release;
                }

                // Assemble P1
                if (inputs.assemble_p1) {
                    inputs.assemble_p1 = false;
                    ROS_INFO(" [Robot] Part 1 assembling in progress");
                    state_ = State::AssembleP1;
                }

                // Assemble P2
                if (inputs.assemble_p2) {
                    inputs.assemble_p2 = false;
                    ROS_INFO(" [Robot] Part 2 assembling in progress");
                    state_ = State::AssembleP2;

                }

                // Assemble P3
                if (inputs.assemble_p3) {
                    inputs.assemble_p3 = false;
                    ROS_INFO(" [Robot] Part 3 assembling in progress");
                    state_ = State::AssembleP3;
                }
                break;

            case State::Right:
                commands.move_right = true;

                // Udpate location
                location_update(current_location);

                // Detect a location changement
                if (!(current_location[0] == previous_location[0] && current_location[1] == previous_location[1] && current_location[2] == previous_location[2])) {
                    commands.move_right = false;
                    location_update(current_location, true);
                    ROS_INFO(" [Robot] Idle");
                    state_ = State::Idle;
                }

                // Send command 
                sendCommands(commands);
                break;

            case State::Left:
                commands.move_left = true;

                /// Udpate location
                location_update(current_location);

                // Detect a location changement
                if (!(current_location[0] == previous_location[0] && current_location[1] == previous_location[1] && current_location[2] == previous_location[2])) {
                    commands.move_right = false;
                    location_update(current_location, true);
                    ROS_INFO(" [Robot] Idle");
                    state_ = State::Idle;
                }

                // Send command 
                sendCommands(commands);
                break;
            
            case State::Grasp:
                commands.grasp = true;
                if (getState().part_grasped) {
                    ROS_INFO(" [Robot] Part grasped");
                    ROS_INFO(" [Robot] Idle");
                    outputs.grasped = true;
                    sendOuputs(outputs);
                    state_ = State::Idle;
                }
                break;

            case State::Release:
                commands.release = true;
                if (getState().part_released) {
                    ROS_INFO(" [Robot] Part released");
                    ROS_INFO(" [Robot] Idle");
                    outputs.released = true;
                    sendOuputs(outputs);
                    state_ = State::Idle;
                }
                break;

            case State::AssembleP1:
                commands.assemble_part1 = true;
                if (getState().part1_assembled) {
                    ROS_INFO(" [Robot] Part 1 assembled");
                    ROS_INFO(" [Robot] Idle");
                    outputs.done_p1 = true;
                    sendOuputs(outputs);
                    state_ = State::Idle;
                }
                break;

            case State::AssembleP2:
                commands.assemble_part2 = true;
                if (getState().part2_assembled) {
                    ROS_INFO(" [Robot] Part 2 assembled");
                    ROS_INFO(" [Robot] Idle");
                    outputs.done_p2 = true;
                    sendOuputs(outputs);
                    state_ = State::Idle;
                }
                break;

            case State::AssembleP3:
                commands.assemble_part3 = true;
                if (getState().part3_assembled) {
                    ROS_INFO(" [Robot] Part 3 assembled");
                    ROS_INFO(" [Robot] Idle");
                    outputs.done_p3 = true;
                    sendOuputs(outputs);
                    state_ = State::Idle;
                }
                break;
        }

        sendCommands(commands);
    }

private:
    enum class State  { Idle, Right, Left, Grasp, Release, AssembleP1, AssembleP2, AssembleP3 };
    State state_;
    bool previous_location[3];
    bool current_location[3];

};

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "robot");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    Robot robot(node);

    while (ros::ok()) {
        robot.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}
