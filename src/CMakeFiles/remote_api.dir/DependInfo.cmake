# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/Users/cyril/Desktop/HMEE303/assembly_control_ros/src/vrep_remote_api/extApi.c" "/Users/cyril/Desktop/HMEE303/assembly_control_ros/src/CMakeFiles/remote_api.dir/vrep_remote_api/extApi.o"
  "/Users/cyril/Desktop/HMEE303/assembly_control_ros/src/vrep_remote_api/extApiCustom.c" "/Users/cyril/Desktop/HMEE303/assembly_control_ros/src/CMakeFiles/remote_api.dir/vrep_remote_api/extApiCustom.o"
  "/Users/cyril/Desktop/HMEE303/assembly_control_ros/src/vrep_remote_api/extApiPlatform.c" "/Users/cyril/Desktop/HMEE303/assembly_control_ros/src/CMakeFiles/remote_api.dir/vrep_remote_api/extApiPlatform.o"
  )
set(CMAKE_C_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "MAX_EXT_API_CONNECTIONS=255"
  "NON_MATLAB_PARSING"
  "__APPLE__"
  "remote_api_EXPORTS"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../include/vrep_remote_api"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
