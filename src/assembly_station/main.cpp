#include <ros/ros.h>

#include <assembly_control_ros/assembly_station_state.h>
#include <assembly_control_ros/assembly_station_command.h>
#include <assembly_control_ros/assembly_station_input.h>
#include <assembly_control_ros/assembly_station_output.h>

#include <common/machine_controller.hpp>

class AssemblyStation
    : public MachineController<assembly_control_ros::assembly_station_state,
                               assembly_control_ros::assembly_station_input,
                               assembly_control_ros::assembly_station_command,
                               assembly_control_ros::assembly_station_output> {
public:
    AssemblyStation(ros::NodeHandle node)
        : MachineController(node, "assembly_station"), state_(State::Free) {
    }

    virtual void process() override {
        assembly_control_ros::assembly_station_command commands;
        assembly_control_ros::assembly_station_output outputs;

        auto& inputs = getInputs();

        switch (state_) {
            case State::Free:
                if (inputs.all_parts_available) {
                    inputs.all_parts_available = false;

                    ROS_INFO("[AssemblyStation] Checking");
                    state_ = State::Checking;
                }
                break;

            case State::Checking:
                commands.check = true;

                if (getState().valid) {

                    ROS_INFO("[AssemblyStation] Checking valid");
                    state_ = State::Evacuation;
                }
                break;

            case State::Evacuation:
                commands.evacuation = true;

                if (getState().evacuated) {
                    outputs.ready = true;
                    sendOuputs(outputs);

                    ROS_INFO("[AssemblyStation] Ready");
                    state_ = State::Free;
                }
                break;
        }

        sendCommands(commands);
    }

private:
    enum class State { Free, Checking, Evacuation };

    State state_;
};

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "assembly_station");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    AssemblyStation assembly(node);

    while (ros::ok()) {
        assembly.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}
