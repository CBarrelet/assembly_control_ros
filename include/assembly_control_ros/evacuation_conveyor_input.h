// Generated by gencpp from file assembly_control_ros/evacuation_conveyor_input.msg
// DO NOT EDIT!


#ifndef ASSEMBLY_CONTROL_ROS_MESSAGE_EVACUATION_CONVEYOR_INPUT_H
#define ASSEMBLY_CONTROL_ROS_MESSAGE_EVACUATION_CONVEYOR_INPUT_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace assembly_control_ros
{
template <class ContainerAllocator>
struct evacuation_conveyor_input_
{
  typedef evacuation_conveyor_input_<ContainerAllocator> Type;

  evacuation_conveyor_input_()
    : restart(false)
    , stop(false)  {
    }
  evacuation_conveyor_input_(const ContainerAllocator& _alloc)
    : restart(false)
    , stop(false)  {
  (void)_alloc;
    }



   typedef uint8_t _restart_type;
  _restart_type restart;

   typedef uint8_t _stop_type;
  _stop_type stop;





  typedef boost::shared_ptr< ::assembly_control_ros::evacuation_conveyor_input_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::assembly_control_ros::evacuation_conveyor_input_<ContainerAllocator> const> ConstPtr;

}; // struct evacuation_conveyor_input_

typedef ::assembly_control_ros::evacuation_conveyor_input_<std::allocator<void> > evacuation_conveyor_input;

typedef boost::shared_ptr< ::assembly_control_ros::evacuation_conveyor_input > evacuation_conveyor_inputPtr;
typedef boost::shared_ptr< ::assembly_control_ros::evacuation_conveyor_input const> evacuation_conveyor_inputConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::assembly_control_ros::evacuation_conveyor_input_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::assembly_control_ros::evacuation_conveyor_input_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace assembly_control_ros

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': True, 'IsMessage': True, 'HasHeader': False}
// {'std_msgs': ['/opt/ros/melodic/share/std_msgs/cmake/../msg'], 'assembly_control_ros': ['/home/cyril/catkin_ws/src/assembly_control_ros/msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::assembly_control_ros::evacuation_conveyor_input_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::assembly_control_ros::evacuation_conveyor_input_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::assembly_control_ros::evacuation_conveyor_input_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::assembly_control_ros::evacuation_conveyor_input_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::assembly_control_ros::evacuation_conveyor_input_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::assembly_control_ros::evacuation_conveyor_input_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::assembly_control_ros::evacuation_conveyor_input_<ContainerAllocator> >
{
  static const char* value()
  {
    return "b49a55abe6d88878d89f764f12a1c489";
  }

  static const char* value(const ::assembly_control_ros::evacuation_conveyor_input_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0xb49a55abe6d88878ULL;
  static const uint64_t static_value2 = 0xd89f764f12a1c489ULL;
};

template<class ContainerAllocator>
struct DataType< ::assembly_control_ros::evacuation_conveyor_input_<ContainerAllocator> >
{
  static const char* value()
  {
    return "assembly_control_ros/evacuation_conveyor_input";
  }

  static const char* value(const ::assembly_control_ros::evacuation_conveyor_input_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::assembly_control_ros::evacuation_conveyor_input_<ContainerAllocator> >
{
  static const char* value()
  {
    return "bool restart\n"
"bool stop\n"
;
  }

  static const char* value(const ::assembly_control_ros::evacuation_conveyor_input_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::assembly_control_ros::evacuation_conveyor_input_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.restart);
      stream.next(m.stop);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct evacuation_conveyor_input_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::assembly_control_ros::evacuation_conveyor_input_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::assembly_control_ros::evacuation_conveyor_input_<ContainerAllocator>& v)
  {
    s << indent << "restart: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.restart);
    s << indent << "stop: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.stop);
  }
};

} // namespace message_operations
} // namespace ros

#endif // ASSEMBLY_CONTROL_ROS_MESSAGE_EVACUATION_CONVEYOR_INPUT_H
