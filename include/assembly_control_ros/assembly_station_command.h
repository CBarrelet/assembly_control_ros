// Generated by gencpp from file assembly_control_ros/assembly_station_command.msg
// DO NOT EDIT!


#ifndef ASSEMBLY_CONTROL_ROS_MESSAGE_ASSEMBLY_STATION_COMMAND_H
#define ASSEMBLY_CONTROL_ROS_MESSAGE_ASSEMBLY_STATION_COMMAND_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace assembly_control_ros
{
template <class ContainerAllocator>
struct assembly_station_command_
{
  typedef assembly_station_command_<ContainerAllocator> Type;

  assembly_station_command_()
    : check(false)
    , evacuation(false)  {
    }
  assembly_station_command_(const ContainerAllocator& _alloc)
    : check(false)
    , evacuation(false)  {
  (void)_alloc;
    }



   typedef uint8_t _check_type;
  _check_type check;

   typedef uint8_t _evacuation_type;
  _evacuation_type evacuation;





  typedef boost::shared_ptr< ::assembly_control_ros::assembly_station_command_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::assembly_control_ros::assembly_station_command_<ContainerAllocator> const> ConstPtr;

}; // struct assembly_station_command_

typedef ::assembly_control_ros::assembly_station_command_<std::allocator<void> > assembly_station_command;

typedef boost::shared_ptr< ::assembly_control_ros::assembly_station_command > assembly_station_commandPtr;
typedef boost::shared_ptr< ::assembly_control_ros::assembly_station_command const> assembly_station_commandConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::assembly_control_ros::assembly_station_command_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::assembly_control_ros::assembly_station_command_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace assembly_control_ros

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': True, 'IsMessage': True, 'HasHeader': False}
// {'std_msgs': ['/opt/ros/melodic/share/std_msgs/cmake/../msg'], 'assembly_control_ros': ['/home/cyril/catkin_ws/src/assembly_control_ros/msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::assembly_control_ros::assembly_station_command_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::assembly_control_ros::assembly_station_command_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::assembly_control_ros::assembly_station_command_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::assembly_control_ros::assembly_station_command_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::assembly_control_ros::assembly_station_command_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::assembly_control_ros::assembly_station_command_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::assembly_control_ros::assembly_station_command_<ContainerAllocator> >
{
  static const char* value()
  {
    return "945c980822be3de90a2bea86ac5d22c4";
  }

  static const char* value(const ::assembly_control_ros::assembly_station_command_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x945c980822be3de9ULL;
  static const uint64_t static_value2 = 0x0a2bea86ac5d22c4ULL;
};

template<class ContainerAllocator>
struct DataType< ::assembly_control_ros::assembly_station_command_<ContainerAllocator> >
{
  static const char* value()
  {
    return "assembly_control_ros/assembly_station_command";
  }

  static const char* value(const ::assembly_control_ros::assembly_station_command_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::assembly_control_ros::assembly_station_command_<ContainerAllocator> >
{
  static const char* value()
  {
    return "bool check\n"
"bool evacuation\n"
;
  }

  static const char* value(const ::assembly_control_ros::assembly_station_command_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::assembly_control_ros::assembly_station_command_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.check);
      stream.next(m.evacuation);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct assembly_station_command_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::assembly_control_ros::assembly_station_command_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::assembly_control_ros::assembly_station_command_<ContainerAllocator>& v)
  {
    s << indent << "check: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.check);
    s << indent << "evacuation: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.evacuation);
  }
};

} // namespace message_operations
} // namespace ros

#endif // ASSEMBLY_CONTROL_ROS_MESSAGE_ASSEMBLY_STATION_COMMAND_H
